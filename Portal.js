import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import './Portal.scss';

/**
 * Portal
 * @description [Description]
 * @example
  <div id="Portal"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Portal, {
        title : 'Example Portal'
    }), document.getElementById("Portal"));
  </script>
 */
class Portal extends React.Component {
	constructor(props) {
		super(props);

		if (!SERVER) {
			this.parent = document.createElement('div');
		}
	}

	onReady = () => {
		const {portalId} = this.props;

		let portalRoot = document.getElementById(portalId);

		if (!portalRoot) {
			portalRoot = document.createElement('div');
			portalRoot.setAttribute('id', portalId);

			document.body.appendChild(portalRoot);
		}

		portalRoot.appendChild(this.parent);
	};

	componentDidMount() {
		document.addEventListener('DOMContentLoaded', this.onReady, false);
	}

	componentWillUnmount() {
		const {portalId} = this.props;
		const portalRoot = document.getElementById(portalId);

		if (portalRoot) {
			portalRoot.removeChild(this.parent);
		}

		document.removeEventListener('DOMContentLoaded', this.onReady);
	}

	render() {
		const {children} = this.props;

		if (SERVER) {
			return null;
		}

		return ReactDOM.createPortal(children, this.parent);
	}
}

Portal.defaultProps = {
	portalId: 'portal',
	children: null
};

Portal.propTypes = {
	portalId: PropTypes.string.isRequired,
	children: PropTypes.node
};

export default Portal;
